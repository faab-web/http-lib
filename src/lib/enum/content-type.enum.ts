export enum ContentTypeEnum {
  APPLICATION_JSON = 'application/json',
  TEXT_PLAIN = 'text/plain'// 'text/plain;charset=UTF-8'
}
