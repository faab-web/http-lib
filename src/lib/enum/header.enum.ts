export enum HeaderEnum {
  CONTENT_TYPE = 'Content-Type',
  X_API_VERSION = 'X-API-VERSION',
  X_OE_APP_PREFIX = 'X-OE-APP-PREFIX',
  X_OE_APP_KEY = 'X-OE-APP-KEY',
  X_OE_UAPP_KEY = 'X-OE-UAPP-KEY',
  X_IGNORE_INTERCEPTOR = 'X-IGNORE-INTERCEPTOR',
  X_AUTHORIZATION = 'Authorization',
  X_TOTAL_COUNT = 'X-TOTAL-COUNT',
  ACCEPT_LANGUAGE = 'Accept-Language',
  EXPIRES = 'Expires',
  LAST_MODIFIED = 'Last-Modified',
  CACHE_CONTROL = 'Cache-Control',
  PRAGMA = 'Pragma'
}
