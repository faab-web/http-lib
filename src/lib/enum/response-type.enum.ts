export enum ResponseTypeEnum {
  JSON = 'json',
  BLOB = 'blob'
}
