export enum ErrorHandlerEnum {
  INTERCEPTION = 'interception',
  UNKNOWN = 'unknown',
  REPEAT = 'repeat'
}
