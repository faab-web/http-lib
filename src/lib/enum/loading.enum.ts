export enum LoadingEnum {
  FULL = 'FULL',
  SMALL = 'SMALL',
  DISABLED = 'DISABLED'
}
