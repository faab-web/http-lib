import { HttpErrorResponse } from '@angular/common/http';
import { ErrorHandlerEnum } from './../enum/error-handler.enum';

export class BaseErrorHandler {
  handle(err: HttpErrorResponse, attempt: number): Promise<ErrorHandlerEnum> {
    return new Promise<ErrorHandlerEnum>(resolve => {
      return ErrorHandlerEnum.UNKNOWN;
    });
  }
  createAnswer(error: ErrorHandlerEnum): Promise<ErrorHandlerEnum> {
    return new Promise<ErrorHandlerEnum>(resolve => {
      return error;
    });
  }
}
