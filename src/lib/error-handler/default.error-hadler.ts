import { ErrorHandlerEnum } from './../enum/error-handler.enum';
import { HttpErrorResponse } from '@angular/common/http';
import { BaseErrorHandler } from './base.error-handler';

export class DefaultErrorHandler extends BaseErrorHandler {

  private max_resend: number = 5;

  constructor(max_resend: number) {
    super();
    if (!isNaN(max_resend)) {
      this.max_resend = max_resend;
    }
  }

  async handle(err: HttpErrorResponse, attempt: number): Promise<ErrorHandlerEnum> {
    if (err.status === 502) {
      if (attempt >= this.max_resend) {
        return this.createAnswer(ErrorHandlerEnum.UNKNOWN);
      }
      return this.createAnswer(ErrorHandlerEnum.REPEAT);
    }

    if (err.status === 403) {
      if (attempt > 1) {
        return this.createAnswer(ErrorHandlerEnum.UNKNOWN);
      }
      // await this.refreshToken();
      return this.createAnswer(ErrorHandlerEnum.REPEAT);
    }

    return this.createAnswer(ErrorHandlerEnum.UNKNOWN);
  }

  async refreshToken() {
    // const token = new Token();
    // token.accessToken = this.storage.getAuthKey();
    // token.user = this.storage.getUser();
    //
    // const result =  await this.http
    //   .server(environment.url_local_be)
    //   .post<Token>('/token/refresh', token, null).toPromise();
    // alert(2);
    // return result;
  }


}
