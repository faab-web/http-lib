import { EnvironmentEnum } from '@faab/base-lib';

export interface HttpOptions {
  environment_prefix: EnvironmentEnum;
  withCredentials?: boolean;
}
