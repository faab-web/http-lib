export interface HttpModuleOptions {
  base_url: string;
  delayLoadingAnimation: number;
  max_resend: number;
  x_api_version: string;
  interceptor?: any;
  authorization?: {
    url_local_be: string;
  };
}
