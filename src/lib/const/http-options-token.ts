import { HttpModuleOptions } from '../interface/http-module-options';
import { InjectionToken } from '@angular/core';

export const HTTP_OPTIONS_TOKEN = new InjectionToken<HttpModuleOptions>('HTTP_OPTIONS_TOKEN');
