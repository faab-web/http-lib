import { HeaderEnum } from './../enum/header.enum';
import {Injectable} from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import {BehaviorSubject, EMPTY, Observable, throwError} from 'rxjs';
import {catchError, filter, finalize, switchMap, take} from 'rxjs/operators';
// import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {LoadingService} from '../service/loading.service';
import {ErrorStatus} from '../enum/error-status.enum';

@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {

  isRefreshingToken: boolean = false;
  tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(
    // public auth: AuthService,
    private http: HttpClient,
    private router: Router,
    private loading: LoadingService
  ) {
  }

  private addToken(req: HttpRequest<any>, token: string): HttpRequest<any> {
    this.tokenSubject.next(token);
    return req.clone({setHeaders: {Authorization: token}});
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next
      .handle(request)
      .pipe(
        catchError(
          error => {
            if (!(error instanceof HttpErrorResponse)) {
              return EMPTY;
            }
            const headers: HttpHeaders = request.headers,
              ignore_error_status = [];
            if(headers.has(HeaderEnum.X_IGNORE_INTERCEPTOR)) {
              const error_status: string = headers.get(HeaderEnum.X_IGNORE_INTERCEPTOR),
                ignore_error_status_arr: string[] = error_status.split(','),
                errors_length: number = ignore_error_status_arr.length;
              for(let i = 0; i < errors_length; i++) {
                const error_string: string = ignore_error_status_arr[i],
                  error_number: number = parseInt(error_string, 10);
                if(!error_string || !(error_string in ErrorStatus)) {
                  continue;
                }
                ignore_error_status.push(parseInt(error_string, 10));
              }
            }
            switch ((<HttpErrorResponse>error).status) {
              case ErrorStatus.UNAUTHORIZED:
                this.loading.show(false);
                if (request.url.search('token/refresh') !== -1) {
                  this.logoutUser(ErrorStatus.UNAUTHORIZED);
                }
                break;
              case ErrorStatus.PAYMENT_REQUIRED:
                if(ignore_error_status.indexOf(ErrorStatus.PAYMENT_REQUIRED) >= 0) {
                  return throwError(error);
                }
                this.logoutUser(ErrorStatus.PAYMENT_REQUIRED);
                break;
              // case 502:
              //   return request.clone();
              // break;
              case ErrorStatus.FORBIDDEN:
                return this.handle403Error(request, next);
              default:
                console.log('error');
                console.log(error);
                console.log('throwError');
                console.log(throwError);
                return throwError(error);
            }
          })
      );
  }

  handle403Error(req: HttpRequest<any>, next: HttpHandler): Observable<never | HttpEvent<any>> {
    if (!this.isRefreshingToken) {

      this.isRefreshingToken = true;

      // Reset here so that the following requests wait until the token
      // comes back from the refreshToken call.
      this.tokenSubject.next(null);

      // const authService = this.injector.get(AuthService);
      // let observable = this.auth.postFrontTokenRefresh();
      // observable.pipe();

      return new Observable();
      /*
      return <Observable<never>>this.auth.postFrontTokenRefresh().pipe(
        switchMap((newToken: string) => {
          if (newToken) {
            this.tokenSubject.next(newToken);
            this.isRefreshingToken = false;
            return next.handle(this.addToken(req, newToken));
          }

          // If we don't get a new token, we are in trouble so logout.
          return this.logoutUser();
        }),
        catchError(error => {
          // If there is an exception calling 'refreshToken', bad news so logout.
          return this.logoutUser();
        }),
        finalize(() => {
          // debugger;
          /// this.isRefreshingToken = false;
        })
        */
      );
    } else {
      return this.tokenSubject.pipe(
        filter(token => token != null),
        take(1),
        switchMap(token => {
          return next.handle(this.addToken(req, token));
        })
      );
    }
  }

  logoutUser(err_status?: ErrorStatus): Observable<never> {
    this.loading.change.emit(false);
    if(!err_status) {
      err_status = ErrorStatus.UNAUTHORIZED;
    }
    setTimeout(() => {
      // this.auth.logout(err_status);
    }, 200);
    return throwError('');

  }

}
