import { HeaderEnum } from './../enum/header.enum';
import { ErrorStatus } from './../enum/error-status.enum';
import { DefaultErrorHandler } from './../error-handler/default.error-hadler';
import { BaseErrorHandler } from './../error-handler/base.error-handler';
import { LoadingService } from './loading.service';
import { HttpClient, HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { StorageService } from '@faab/storage-lib';
import { HttpModuleOptions } from '../interface/http-module-options';
import { HTTP_OPTIONS_TOKEN } from './../const/http-options-token';
import { ResponseTypeEnum } from './../enum/response-type.enum';
import { ContentTypeEnum } from './../enum/content-type.enum';
import { LoadingEnum } from './../enum/loading.enum';
import { HttpOptions } from './../interface/http-options';
import { Injectable, Inject } from '@angular/core';
import { LanguageLibService } from '@faab/language-lib';
import { Observable } from 'rxjs';
import { ResponseInterfaces } from '../interface/response-interfaces';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private http_options: HttpOptions;
  private loadingType: LoadingEnum = LoadingEnum.FULL;
  private contentType: ContentTypeEnum = ContentTypeEnum.APPLICATION_JSON;
  private _responseType: ResponseTypeEnum = ResponseTypeEnum.JSON;
  private baseUrl: string = '';
  private _ignore: string;
  private auth: boolean = true;
  private _cache: boolean = true;
  private _withCredentials: boolean = false;
  private errorHandler: BaseErrorHandler;

  private config: HttpModuleOptions;

  constructor(
    @Inject(HTTP_OPTIONS_TOKEN) private token_config: HttpModuleOptions,
    private storage: StorageService,
    private http: HttpClient,
    private loadingService: LoadingService,
    private languageService: LanguageLibService
  ) {
    this.config = token_config;
    if (typeof token_config.base_url === 'string') {
      this.baseUrl = token_config.base_url;
    }
    const max_resend: number = isNaN(token_config.max_resend) ? 5 : token_config.max_resend;
    this.errorHandler = new DefaultErrorHandler(max_resend);
  }

  createUrl(url: string) {
    if (url != null && url.length > 0) {
      if (url.charAt(0) !== '/') {
        url = '/' + url;
      }
    }
    return this.baseUrl + url;
  }

  private showLoading(show: boolean): void {
    if (this.loadingType === LoadingEnum.DISABLED) {
      return;
    }

    this.loadingService.show(show);
  }

  private reset() {
    this.contentType = ContentTypeEnum.TEXT_PLAIN;
    this._responseType = ResponseTypeEnum.JSON;
    this._ignore = '';
    this.baseUrl = (typeof this.config.base_url === 'string') ? this.config.base_url : '';
    this.auth = true;
    // this.loadingType = LoadingEnum.FULL;
    this._withCredentials = false;
  }

  responseType(type: ResponseTypeEnum) {
    this._responseType = type;
    return this;
  }

  content(type: ContentTypeEnum): HttpService {
    this.contentType = type;
    return this;
  }

  handler(handler: BaseErrorHandler) {
    this.errorHandler = handler;
    return this;
  }

  loading(loadingType: LoadingEnum): HttpService {
    this.loadingType = loadingType;
    return this;
  }

  server(server: string): HttpService {
    this.baseUrl = server;
    this._withCredentials = false;
    return this;
  }

  ignore(errors: ErrorStatus[]): HttpService {
    if (!errors || !Array.isArray(errors)) {
      return this;
    }
    this._ignore = errors.join(',');
    return this;
  }

  cache(use: boolean): HttpService {
    this._cache = use;
    return this;
  }

  withCredentials(withCredentials: boolean): HttpService {
    this._withCredentials = withCredentials;
    return this;
  }

  get<T>(shortUrl: string, response: ResponseInterfaces<T>): Observable<any> | Promise<any> {
    return this.request('GET', this.createUrl(shortUrl), this.createOptions(), response);
  }

  delete(shortUrl: string, response: ResponseInterfaces<any>): Observable<any> | Promise<any> {
    this.contentType = ContentTypeEnum.TEXT_PLAIN; // 'text/plain;charset=UTF-8'

    return this.request('DELETE', this.createUrl(shortUrl), this.createOptions(), response);
  }

  request<T>(method: string, url: string, options, response, iterator: number = 0, sync?: boolean): Observable<any> | Promise<any> {
    // const loadingType = this.loadingType;
    // options.withCredentials = false;
    if (method !== 'DELETE') {
      this.reset();
    }
    if (sync) {
      this.showLoading(false);
      return this.http.request(method, url, options).toPromise();
    }

    const self = this;
    const timer = setTimeout(function () {
      self.showLoading(true);
    }, this.config.delayLoadingAnimation);

    const observable = this.http.request(method, url, options);
    observable.subscribe(
      (next) => {
        clearTimeout(timer);
        self.showLoading(false);
        if (next instanceof HttpResponse) {
          response.success(next.body, next);
          return;
        }
        response.success(next, null);
      },
      (err: HttpErrorResponse) => {
        if (err.status === ErrorStatus.BAD_GATEWAY && iterator <= 10) {
          setTimeout(function () {
            self.request(method, url, options, response, ++iterator, sync);
          }, 500);
          return;
        }
        clearTimeout(timer);
        self.showLoading(false);
        if (response && typeof response.failed === 'function') {
          response.failed(err);
        }
      },
      () => {
        clearTimeout(timer);
        this.showLoading(false);
      }
    );
    return observable;
  }

  post<T>(shortUrl: string, obj, response: ResponseInterfaces<T>, sync?: boolean): Observable<any> | Promise<any> {
    return this.request('POST', this.createUrl(shortUrl), this.createOptions(obj), response, 0, sync);
  }

  put<T>(shortUrl: string, obj: object, response: ResponseInterfaces<T>, sync?: boolean): Observable<any> | Promise<any> {
    return this.request('PUT', this.createUrl(shortUrl), this.createOptions(obj), response, 0, sync);
  }

  patch<T>(shortUrl: string, obj: object, response: ResponseInterfaces<T>, sync?: boolean): Observable<any> | Promise<any> {
    return this.request('PATCH', this.createUrl(shortUrl), this.createOptions(obj), response, 0, sync);
  }

  private createOptions(body?: any): any {
    if (typeof this._withCredentials !== 'boolean') {
      this._withCredentials = false;
    }


    /* if (this._withCredentials) {
      this.http_options.withCredentials = this._withCredentials;
      return this.http_options;
    } */

    let header = new HttpHeaders();
    if (this.contentType) {
      header = header.append(HeaderEnum.CONTENT_TYPE, this.contentType);
    }
    if (this._ignore) {
      header = header.append(HeaderEnum.X_IGNORE_INTERCEPTOR, this._ignore);
    }
    header = header.append(HeaderEnum.ACCEPT_LANGUAGE, this.languageService.getCurrentLanguage());


    const date = new Date();
    header = header.append(HeaderEnum.ACCEPT_LANGUAGE, 'Wed 11 Jan 1984 05:00:00 GMT'); // need to safari
    header = header.append(HeaderEnum.LAST_MODIFIED, date.toISOString()); // need to safari
    header = header.append(HeaderEnum.CACHE_CONTROL, 'no-cache, no-store, must-revalidate, post-check=0, pre-check=0", false');
    header = header.append(HeaderEnum.PRAGMA, 'no-cache');
    // if (!this._cache) {
    //   header = header.append('Cache-Control', 'no-cache, no-store, must-revalidate');
    // } else {
    //   header = header.append('Cache-Control', 'max-age=10, must-revalidate');
    // }

    const uapp_key: string = this.getUappKey();
    if (uapp_key) {
      header = header.append(HeaderEnum.X_OE_UAPP_KEY, uapp_key);
    }


    if (this.config) {
      if (typeof this.config.x_api_version === 'string') {
        header = header.append(HeaderEnum.X_API_VERSION, this.config.x_api_version);
      }

      const app_key: string = this.storage.getAppKey();
      if (app_key) {
        header = header.append(HeaderEnum.X_OE_APP_KEY, app_key);
      }

      if (this.config.authorization) {
        const prefix_public: string = this.storage.getPrefixPublic();
        if (
          typeof this.config.authorization.url_local_be === 'string'
          && this.config.authorization.url_local_be === this.baseUrl
          && typeof prefix_public === 'string'
        ) {
          header = header.append(HeaderEnum.X_OE_APP_PREFIX, prefix_public);
        }

        const authorization: string = this.getAuthorization();
        if (authorization && this.auth) {
          header = header.append(HeaderEnum.X_AUTHORIZATION, authorization);
        }
      }
    }

    return {
      body: body,
      headers: header,
      observe: 'response',
      withCredentials: this._withCredentials,
      responseType: this._responseType
    };

  }

  protected setBaseUrl(): void { }

  getAuthorization(): string {
    return this.storage.getCookiePrivate(HeaderEnum.X_AUTHORIZATION);
  }

  setAuthorization(authorization: string): void {
    this.storage.setCookiePrivate(HeaderEnum.X_AUTHORIZATION, authorization);
  }

  getUappKey(): string {
    return this.storage.getCookie(HeaderEnum.X_OE_UAPP_KEY);
  }

  setUappKey(uapp_key: string): void {
    this.storage.setCookie(HeaderEnum.X_OE_UAPP_KEY, uapp_key);
  }

}
