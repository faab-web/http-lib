import {EventEmitter, Injectable, Output} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  @Output() change: EventEmitter<boolean> = new EventEmitter();

  show(show: boolean) {
    this.change.emit(show);
  }
}
