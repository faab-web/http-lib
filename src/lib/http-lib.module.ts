import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpModuleOptions } from './interface/http-module-options';
import { HTTP_OPTIONS_TOKEN } from './const/http-options-token';
import { HttpService } from './service/http-lib.service';
import { StorageService } from '@faab/storage-lib';
import { Provider } from '@angular/core/src/render3/jit/compiler_facade_interface';

@NgModule({
  declarations: [],
  imports: [
    HttpClientModule
  ],
  exports: []
})
export class HttpLibModule {
  static forRoot(options?: HttpModuleOptions): ModuleWithProviders {
    const http_providers: Provider[] = [
      {
        provide: HTTP_OPTIONS_TOKEN,
        useValue: options,
      },
      StorageService,
      HttpService
    ];
    return {
      ngModule: HttpLibModule,
      providers: http_providers/* [
        {
          provide: HTTP_OPTIONS_TOKEN,
          useValue: options,
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RefreshTokenInterceptor,
          multi: true
        },
        StorageService,
        HttpService
      ] */,
    };
  }
}
